#!/bin/bash

# Als Rotationstemperatur wird 30K angenommen, es sei denn, die Option -T wird angegeben
# Als Gaußbreite wird 1.0cm-1 angenommen, es sei denn, die Option -s wird angegeben
# wenn -pt, dann wird nur der Poltensor berechnet und geschrieben, ohne Rotationssimulation
# wenn -pl, dann werden die Rovib-Linien geschrieben, ohne Glättung
# wenn -nr, dann werden Wellenzahlen nicht gerundet und die Glättung wird langsamer

if [ ! -f dxyz.out ]; then
        echo "Run raman_rotvib_prepare.sh first!"
        exit 1
fi

TROT=""
SIG=""
PRINT_TENSOR=""
PRINT_LINES=""
NOT_ROUND=""

while [[ $# -gt 0 ]]
do
key="$1"

case $key in
        -T)
        TROT=", 'Trot', $2"
        echo Bash: non-default rotational T $2 K recognized.
        shift
        shift
        ;;
        -s)
        SIG=", 'sig', $2"
        echo Bash: non-default Gauss width $2 cm-1 recognized.
        shift
        shift
        ;;
        -pt)
        PRINT_TENSOR=", print_tensor"
        echo Bash: printing tensor without rovib simulation.
        shift
        ;;
        -pl)
        PRINT_LINES=", print_lines"
        echo Bash: printing lines without Gauss broadening.
        shift
        ;;
        -nr)
        NOT_ROUND=", dont_round"
        echo Bash: no rounding of wavenumbers prior to Gauss broadening.
        shift
        ;;
        *)
        echo "Invalid option, use -T TROT, -s SIGMA, -pt, -pl or -nr"
        exit 1
        ;;
esac
done

# Jetzt kann Octave aufgerufen werden, das die fünf Dateien als Input nimmt und die neue Datei "Vibrot_{T}K_{s}cm.dat" mit 40001 Zeilen und 3 Spalten (Wellenzahl, Intensität bei 90° Laserpolarisation, Int bei 0°) schreibt. Die vibrot_int liegt in $HOME/bin.

octave -q -p $HOME/bin --eval "rotvib_int('wavenumbers.out', 'vibmodes.out', 'reduced_masses.out', 'dxyz.out', 'rot_constants.out' $TROT $SIG $PRINT_TENSOR $PRINT_LINES $NOT_ROUND)"

#rm wavenumbers.out vibmodes.out reduced_masses.out dxyz.out rot_constants.out

exit 0
