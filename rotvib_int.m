function rotvib_int(varargin)

% Es wird min. Octave Version 4 benötigt
if compare_versions (version, "4.0.0", "<")
  error("Octave Version 4 or newer required. Aborting.\n");
end

% Rotationssimulation mit korrigierten alpha-/gamma-Beiträgen aus Raumwinkelintegration; mit verbesserter Kontrolle der Inputs

p = inputParser();
p.FunctionName = "rotvib_parser";
% Test der Inputfiles: Sind sie in der richtigen Reihenfolge gegeben (anhand des Dateinamens geprüft) und sind das überhaupt Dateien (isfile)
val_wn = @(x) strfind(x, "wavenumber") && isfile(x);
p.addRequired("Wavenumbers_filename", val_wn);
val_vibmodes = @(x) strfind(x, "vibmode") && isfile(x);
p.addRequired("Vibmodes_filename", val_vibmodes);
val_masses = @(x) strfind(x, "masses") && isfile(x);
p.addRequired("Reduced_masses_filename", val_masses);
val_dxyz = @(x) strfind(x, "dxyz") && isfile(x);
p.addRequired("dxyz_filename", val_dxyz);
val_rotconst = @(x) strfind(x, "rot") && isfile(x);
p.addRequired("Rotconst_filename", val_rotconst);

% Rotationstemperatur und Gaußbreite zur Glättung/Verbreiterung als Parameter, können in beliebiger Reihenfolge nach den Dateinamen angegeben werden
p.addParameter("Trot", 30, @isnumeric);
p.addParameter("sig", 1.0, @isnumeric);

% Sollen Rotationsinien ohne Glättung ausgegeben werden?
% Sollen die Komponenten des Tensors ohne Rovibsimulation ausgegeben werden?
% Sollen die Linien nicht gerundet (und zusammengefasst) werden? Standard ist runden, damit die Gaußverbreiterung schneller geht (per default sind switches immer false, daher die doppelte Verneinung)
p.addSwitch("print_lines");
p.addSwitch("print_tensor");
p.addSwitch("dont_round");

% alle Inputs checken
p.parse(varargin{:});
% einzelne Elemente können nun z.B. mit p.Results.Wavenumbers_filename aufgerufen werden


% Zeitmessung, am Ende des Skripts steht das dazugehörige 'toc'
tic;

% Nervige Warnung ausschalten
warning("off","Octave:divide-by-zero")
warning("off","Octave:broadcast");

% Naturkonstanten und konstante Vorfaktoren in SI

h = 6.62607015e-34; c = 2.99792458e8; a0 = 5.29177210903e-11; me = 9.1093837015e-31; kb = 1.380649e-23; mu = 1.66053906660e-27;
amu2au=mu/me;
laser = 532.27e-9;
# Vibrationstemperatur als Mittelwert von 20 und 180K
fact = 2*pi^2*h*a0^4/(45*c*me*laser);
exp_fact = h*c/(kb);

% Einlesen der reduzierten Massen, die zur Rückrechnung der massengewichteten Normalmoden benötigt werden
reduced_masses_input=load(p.Results.Reduced_masses_filename);

% Reduzierte Massen sind ein Zeilenvektor
reduced_masses=sqrt(reduced_masses_input*amu2au);

% Einlesen der Normalmoden, die Spalten sind x1 y1 z1 x2 y2..., die Zeilen sind die jeweiligen Normalmoden vib1 vib2...
vibmodes_input=load(p.Results.Vibmodes_filename);

% Zu jeder Schwingung gibt es eine eigene red. Masse, daher werden alle Elemente einer Zeile durch den selben Wert dividiert
vibmod=(vibmodes_input./reduced_masses');

% Die Komponenten der kartesischen Ableitung der Polarisierbarkeit werden eingelesen. Diese enthält 6 Zeilen in der Reihenfolge xx, yy, zz, xy, xz, yz. In den Spalten stehen x1 y1 z1 x2 y2 z2...
dxyz = load(p.Results.dxyz_filename);

% Die Ableitungen werden mit den Normalmoden multipliziert. In dxyz stehen die Kernkoordinaten in einer Zeile, weshalb sie in vibmod in einer Spalte stehen müssen – daher die Invertierung von vibmod. Das Ergebnis wird wiederum invertiert, sodass in jeder Zeile die Komponenten einer Schwingung stehen. Außerdem werden die ersten 6 Zeilen übersprungen, da diese Rot + Trans sind.
Poltensors=((dxyz*vibmod')')(7:end,:);

% Schwingungswellenzahlen und Komponenten der Übergangspolarisierbarkeit werden zusammengeführt.
Vibrations = [load(p.Results.Wavenumbers_filename), Poltensors];

% wenn Poltensor ausgegeben werden soll, wird er in eine Datei geschrieben und die Funktion hier beendet

if p.Results.print_tensor
  dlmwrite("poltensors.out", Vibrations, "\t");
  return
end

% Rotationskonstanten werden eingelesen und aufsteigend sortiert (für den Fall, dass sie im Input falsch sortiert sind - da gab's bei Turbomole wohl in der Vergangenheit mal Probleme mit)
% Rays Kappa liegt zwischen -1 und +1 und gibt die Natur des Kreisels an: -1 = prolate (gestreckt), +1 = oblate (abgeplättet). prolate hat eine große Rotkonstante (A) und zwei identische kleine (B). Da hier als Näherung für asymmetrische Kreisel, wird B als Durchschnitt aus zwei kleineren berechnet. Umgekehrt für oblate.

Rotconst = sort(load(p.Results.Rotconst_filename));
A_inp = Rotconst(3);
B_inp = Rotconst(2);
C_inp = Rotconst(1);
Rays_kappa = (2*B_inp-A_inp-C_inp)/(A_inp-C_inp);
oblate=prolate=0;
if Rays_kappa<0
  % prolate, d.h. B=C
  prolate=1;
  A = A_inp;
  B = (B_inp+C_inp)/2;
else
  % oblate, d.h. A=B. Für die Formeln muss A durch C ersetzt werden (wie im Long)
  oblate=1;
  A = C_inp;
  B = (A_inp+B_inp)/2;
endif

% Haufen an "anonymen Funktionen": name = @(Variable(n)) Ausdruck
% Aufruf einfach als name(Wert)
% Dabei überall elementweise Operationen, damit kein Murks rauskommt, wenn Vektor eingesetzt

% Beitrag der Schwingungszustandssumme, ohne alpha und gamma und auch ohne ny_scattered^3. Da aus Vibrationstheorie, bleibt als ny die Fundamentalschwingung in cm-1; Faktor 100 wegen Umrechnung in m^-1.

raman_vib_cross = @(ny) fact./(100*ny) .*0.5 .*(1./(1-exp(-exp_fact.*100*ny/180)) + 1./(1-exp(-exp_fact.*100*ny/20)));

% Zusammenrechnen mit Streuwellenzahl^3
raman_cross = @(ny_rotvib,ny_vib) (1/laser-100*ny_rotvib).^3.*raman_vib_cross(ny_vib);

% Bei Bedarf können aus den Komponenten des Pol-Tensors die Invarianten alpha^2 und gamma^2 wieder berechnet werden:

alpha_sq = @(xx,yy,zz) 1/9*(xx+yy+zz).^2;
gamma_sq = @(xx,yy,zz,xy,xz,yz) 1/2*((xx-yy).^2 + (xx-zz).^2 + (yy-zz).^2) + 3*(xy.^2 + xz.^2 + yz.^2);

% Die einzelnen Summanden in gamma^2 tragen zu unterschiedlichen dK-Übergängen bei: xz und yz zu dK=1, Terme mit zz zu dK=0, xy zu dK=2. xx^2, yy^2 und xxyy sind nicht sauber separiert und tragen sowohl zu dK=0 und dK=2 bei. Das Verhältnis der Beiträge zu dK0 und dK=2 wird durch einen obskuren Faktor gebildet; R0 und R2 ergeben in Summe wieder 1. Negative Vorzeichen sind böse! Daher 1+ unterm Bruchstrich und die Betragsstriche.

R0 = @(xx,yy,zz) max(1./(1+abs((sqrt(3)*(xx-yy))./(2*zz-xx-yy))),0);
R2 = @(xx,yy,zz) 1-R0(xx,yy,zz);

gamma_sq_dK0 = @(xx,yy,zz) zz.*(zz-xx-yy) + R0(xx,yy,zz).*(xx.^2+yy.^2-xx.*yy);
gamma_sq_dK1 = @(xz,yz) 3*(xz.^2 + yz.^2);
gamma_sq_dK2 = @(xx,yy,zz,xy) R2(xx,yy,zz).*(xx.^2+yy.^2-xx.*yy) + 3*xy.^2;

%[Vibrations(:,1), R0(Vibrations(:,2),Vibrations(:,3),Vibrations(:,4))]


% Korrekturfunktion der CCD
% Da dies von der real gestreuten Frequenz abhängt, wird später die exakte rovibronische Wellenzahl eingesetzt.
% Neue Kalibrierfunktion, die auf 2000px gestützt ist
curry_func = @(x) ...
  (1.7654 ...
  +2.6970e-10*(x-2000).^3 ...
  +0.4316e-13*(x-2000).^4 ...
  -1.1285e-16*(x-2000).^5 ...
  -0.1278e-19*(x-2000).^6 ...
  +0.1926e-22*(x-2000).^7 ...
  +0.1029e-26*(x-2000).^8 ...
  -1.1527e-30*(x-2000).^9); ...

% Placzek-Teller-Faktoren für unterschiedliche Delta K. Beachte: Im O- und P-Zweig ist J>=2! (es gilt J'+J''>=2)
% Zur Nomenklatur: p2 bedeutet Delta K = +2, p1 = +1, p0 = 0, m1 = -1, m2 = -2.
% Es sind Tests eingebaut, die verhindern, dass durch den Übergang ein illegaler Zustand erreicht wird (z.B. mit K>J oder K<0). Ebenso wird getestet, dass der Ausgangszustand erlaubt ist (J>=K). Die Placzek-Teller-Faktoren tun das nicht alle intrinsisch.
% Manchmal tauchen Ausdrücke mit der Form 0^2/0 auf, die (analytisch) gekürzt werden könnten und dann 0 ergäben. Da Octave stupide rechnet, taucht an diesen Stellen NaN als Ergebnis auf. Das wird durch den Ausdruck max(...,0) ersetzt durch 0.
Pla_Op2 = @(J,K) max((J>=K+4).*(J-K-3).*(J-K-2).*(J-K-1).*(J-K)./(4.*J.*(J-1).*(2.*J-1).*(2.*J+1)),0);
Pla_Op1 = @(J,K) max((J>=K+3).*((J-1).^2-(K+1).^2).*((J-K-1).*(J-K))./(J.*(J-1).*(2.*J-1).*(2.*J+1)),0);
Pla_Op0 = @(J,K) max((J>=K+2).*3.*((J-1).^2-K.^2).*(J.^2-K.^2)./(2.*J.*(J-1).*(2.*J-1).*(2.*J+1)),0);
Pla_Om1 = @(J,K) max((J>=K+1).*(K>=1).*((J-1).^2-(K-1).^2).*((J+K-1).*(J+K))./(J.*(J-1).*(2.*J-1).*(2.*J+1)),0);
Pla_Om2 = @(J,K) max((J>=K).*(K>=2).*(J+K-3).*(J+K-2).*(J+K-1).*(J+K)./(4.*J.*(J-1).*(2.*J-1).*(2.*J+1)),0);

Pla_Pp2 = @(J,K) max((J>=K+3).*(J.^2-(K+1).^2).*(J-K-2).*(J-K)./(2.*J.*(J-1).*(J+1).*(2.*J+1)),0);
Pla_Pp1 = @(J,K) max((J>=K+2).*(J+2.*K+1).^2.*(J-K-1).*(J-K)./(2.*J.*(J-1).*(J+1).*(2.*J+1)),0);
Pla_Pp0 = @(J,K) max((J>=K+1).*3.*K.^2.*(J.^2-K.^2)./(J.*(J-1).*(J+1).*(2.*J+1)),0);
Pla_Pm1 = @(J,K) max((J>=K).*(K>=1).*(J-2.*K+1).^2.*(J+K-1).*(J+K)./(2.*J.*(J-1).*(J+1).*(2.*J+1)),0);
Pla_Pm2 = @(J,K) max((J>=K).*(K>=2).*(J.^2-(K-1).^2).*(J+K-2).*(J+K)./(2.*J.*(J-1).*(J+1).*(2.*J+1)),0);

Pla_Qp2 = @(J,K) max((J>=K+2).*3.*(J.^2-(K+1).^2).*((J+1).^2-(K+1).^2)./(2.*J.*(J+1).*(2.*J-1).*(2.*J+3)),0);
Pla_Qp1 = @(J,K) max((J>=K+1).*3.*(2.*K+1).^2.*(J-K).*(J+K+1)./(2.*J.*(J+1).*(2.*J-1).*(2.*J+3)),0);
Pla_Qp0 = @(J,K) max((J>=K).*(J>=1).*(J.*(J+1)-3.*K.^2).^2./(J.*(J+1).*(2.*J-1).*(2.*J+3)),0);
Pla_Qm1 = @(J,K) max((J>=K).*(K>=1).*3.*(2.*K-1).^2.*(J+K).*(J-K+1)./(2.*J.*(J+1).*(2.*J-1).*(2.*J+3)),0);
Pla_Qm2 = @(J,K) max((J>=K).*(K>=2).*3.*(J.^2-(K-1).^2).*((J+1).^2-(K-1).^2)./(2.*J.*(J+1).*(2.*J-1).*(2.*J+3)),0);

Pla_Rp2 = @(J,K) max((J>=K+1).*((J+1).^2-(K+1).^2).*(J+K+1).*(J+K+3)./(2.*J.*(J+1).*(J+2).*(2.*J+1)),0);
Pla_Rp1 = @(J,K) max((J>=K).*(J>=1).*(J-2.*K).^2.*(J+K+1).*(J+K+2)./(2.*J.*(J+1).*(J+2).*(2.*J+1)),0);
Pla_Rp0 = @(J,K) max((J>=K).*(J>=1).*3.*K.^2.*((J+1).^2-K.^2)./(J.*(J+1).*(J+2).*(2.*J+1)),0);
Pla_Rm1 = @(J,K) max((J>=K).*(K>=1).*(J+2.*K).^2.*(J-K+1).*(J-K+2)./(2.*J.*(J+1).*(J+2).*(2.*J+1)),0);
Pla_Rm2 = @(J,K) max((J>=K).*(K>=2).*((J+1).^2-(K-1).^2).*(J-K+1).*(J-K+3)./(2.*J.*(J+1).*(J+2).*(2.*J+1)),0);

Pla_Sp2 = @(J,K) max((J>=K).*(J+K+1).*(J+K+2).*(J+K+3).*(J+K+4)./(4.*(J+1).*(J+2).*(2.*J+1).*(2.*J+3)),0);
Pla_Sp1 = @(J,K) max((J>=K).*((J+1).^2-K.^2).*(J+K+2).*(J+K+3)./((J+1).*(J+2).*(2.*J+1).*(2.*J+3)),0);
Pla_Sp0 = @(J,K) max((J>=K).*3.*((J+1).^2-K.^2).*((J+2).^2-K.^2)./(2.*(J+1).*(J+2).*(2.*J+1).*(2.*J+3)),0);
Pla_Sm1 = @(J,K) max((J>=K).*(K>=1).*((J+1).^2-K.^2).*(J-K+2).*(J-K+3)./((J+1).*(J+2).*(2.*J+1).*(2.*J+3)),0);
Pla_Sm2 = @(J,K) max((J>=K).*(K>=2).*(J-K+1).*(J-K+2).*(J-K+3).*(J-K+4)./(4.*(J+1).*(J+2).*(2.*J+1).*(2.*J+3)),0);

% Wellenzahl jedes J-Übergangs
ny_O = @(J,ny) ny-4*B*(J-0.5);
ny_P = @(J,ny) ny-2*B*J;
ny_Q = @(ny) ny;
ny_R = @(J,ny) ny+2*B*(J+1);
ny_S = @(J,ny) ny+4*B*(J+1.5);

%Wellenzahl eines K-Übergangs
ny_Kp2 = @(K) 4*(A-B)*(K+1);
ny_Kp1 = @(K) 2*(A-B)*(K+0.5);
ny_Kp0 = @(K) 0;
ny_Km1 = @(K) -2*(A-B)*(K-0.5);
ny_Km2 = @(K) -4*(A-B)*(K-1);

% Definition der Besetzungszahlen

% Entartungsgrad in J ist (2*J+1) für J. In K ist er zwei wenn K>=1 und eins wenn K=0.
  gJK = @(J,K) ((K>0)+1).*(2*J+1);

% Besetzungszahl im Verhältnis zum Grundzustand 0,0
% K kann nicht größer sein als J; Besetzungszahlen mit K>J sind exakt 0.
  Nrot_rel = @(J,K,Trot) (J>=K).*gJK(J,K).*exp(-(B*J.*(J+1)+(A-B).*K.^2)*100*h*c./(kb*Trot));

% Die Zweige sollen nur aus Linien mit den J und K bestehen, die wenigstens zu 0.01 des Grundzustandes besetzt sind.
% Fallunterscheidung: Bei prolate steigt die Eigenenergie mit wachsendem K, d.h. kleine K sind stärker besetzt. Für Jmax ist das der Zustand mit K=1, für Kmax ist das J=K. Das Niveau Jmax,Kmax ist dann zwar definitiv weniger als zu 1% besetzt, aber allzu tragisch ist das nicht.
% Bei oblate hingegen ist Jmax=Kmax
Trot = p.Results.Trot;
  Jmax=Kmax=1;
if prolate
  while Nrot_rel(Jmax+1,1,Trot)>0.01; Jmax++; end
  while Nrot_rel(Kmax+1,Kmax+1,Trot)>0.01; Kmax++; end
endif
if oblate
  while Nrot_rel(Jmax+1,Jmax+1,Trot)>0.01; Jmax++; end
  Kmax=Jmax;
endif


% Die Rotationszustandsumme wird numerisch aufsummiert, um die Besetzungszahlen darauf zu normieren. Damit ist sichergestellt, dass die Summe aller Besetzungszahlen wieder 1 ergibt.

  Zrot = sum(sum(Nrot_rel([0:Jmax]',[0:Kmax],Trot)));

  Nrot = @(J,K) Nrot_rel(J,K,Trot)/Zrot;

% Intensität der einzelnen Linien. Weil später alpha^2 und gamma^2 eingesetzt werden, hier ohne Quadrierung geschrieben.
% Intensität bei lamda/2=90°: 4.113304+3.114244/curry_func gamma^2
% Im Qp0-Zweig wird zwischen dem alpha- und gamma-Beitrag unterschieden: Die Gewichtung von Alpha beträgt immer 1, für die Gewichtung der Gamma-Beiträge zu den unterschiedlichen dK-Zweigen ist es aber von Vorteil, erst zum Schluss das Alpha mit einzurechnen.

Int_Op2 = @(J,K,ny,Gamma) Nrot(J,K).*raman_cross(ny_O(J,ny)+ny_Kp2(K),ny).*Pla_Op2(J,K).*Gamma.*(4.113304+3.114244./curry_func(ny_O(J,ny)+ny_Kp2(K)));
Int_Op1 = @(J,K,ny,Gamma) Nrot(J,K).*raman_cross(ny_O(J,ny)+ny_Kp1(K),ny).*Pla_Op1(J,K).*Gamma.*(4.113304+3.114244./curry_func(ny_O(J,ny)+ny_Kp1(K)));
Int_Op0 = @(J,K,ny,Gamma) Nrot(J,K).*raman_cross(ny_O(J,ny)+ny_Kp0(K),ny).*Pla_Op0(J,K).*Gamma.*(4.113304+3.114244./curry_func(ny_O(J,ny)+ny_Kp0(K)));
Int_Om1 = @(J,K,ny,Gamma) Nrot(J,K).*raman_cross(ny_O(J,ny)+ny_Km1(K),ny).*Pla_Om1(J,K).*Gamma.*(4.113304+3.114244./curry_func(ny_O(J,ny)+ny_Km1(K)));
Int_Om2 = @(J,K,ny,Gamma) Nrot(J,K).*raman_cross(ny_O(J,ny)+ny_Km2(K),ny).*Pla_Om2(J,K).*Gamma.*(4.113304+3.114244./curry_func(ny_O(J,ny)+ny_Km2(K)));

Int_Pp2 = @(J,K,ny,Gamma) Nrot(J,K).*raman_cross(ny_P(J,ny)+ny_Kp2(K),ny).*Pla_Pp2(J,K).*Gamma.*(4.113304+3.114244./curry_func(ny_P(J,ny)+ny_Kp2(K)));
Int_Pp1 = @(J,K,ny,Gamma) Nrot(J,K).*raman_cross(ny_P(J,ny)+ny_Kp1(K),ny).*Pla_Pp1(J,K).*Gamma.*(4.113304+3.114244./curry_func(ny_P(J,ny)+ny_Kp1(K)));
Int_Pp0 = @(J,K,ny,Gamma) Nrot(J,K).*raman_cross(ny_P(J,ny)+ny_Kp0(K),ny).*Pla_Pp0(J,K).*Gamma.*(4.113304+3.114244./curry_func(ny_P(J,ny)+ny_Kp0(K)));
Int_Pm1 = @(J,K,ny,Gamma) Nrot(J,K).*raman_cross(ny_P(J,ny)+ny_Km1(K),ny).*Pla_Pm1(J,K).*Gamma.*(4.113304+3.114244./curry_func(ny_P(J,ny)+ny_Km1(K)));
Int_Pm2 = @(J,K,ny,Gamma) Nrot(J,K).*raman_cross(ny_P(J,ny)+ny_Km2(K),ny).*Pla_Pm2(J,K).*Gamma.*(4.113304+3.114244./curry_func(ny_P(J,ny)+ny_Km2(K)));

Int_Qp2 = @(J,K,ny,Gamma) Nrot(J,K).*raman_cross(ny_Q(ny)+ny_Kp2(K),ny).*Pla_Qp2(J,K).*Gamma.*(4.113304+3.114244./curry_func(ny_Q(ny)+ny_Kp2(K)));
Int_Qp1 = @(J,K,ny,Gamma) Nrot(J,K).*raman_cross(ny_Q(ny)+ny_Kp1(K),ny).*Pla_Qp1(J,K).*Gamma.*(4.113304+3.114244./curry_func(ny_Q(ny)+ny_Kp1(K)));
Int_Qp0 = @(J,K,ny,Gamma) Nrot(J,K).*raman_cross(ny_Q(ny)+ny_Kp0(K),ny).*Pla_Qp0(J,K).*Gamma.*(4.113304+3.114244./curry_func(ny_Q(ny)+ny_Kp0(K)));
Int_Qp0_alpha = @(ny,Alpha) raman_cross(ny_Q(ny),ny).*Alpha.*(45+0.042325./curry_func(ny));
Int_Qm1 = @(J,K,ny,Gamma) Nrot(J,K).*raman_cross(ny_Q(ny)+ny_Km1(K),ny).*Pla_Qm1(J,K).*Gamma.*(4.113304+3.114244./curry_func(ny_Q(ny)+ny_Km1(K)));
Int_Qm2 = @(J,K,ny,Gamma) Nrot(J,K).*raman_cross(ny_Q(ny)+ny_Km2(K),ny).*Pla_Qm2(J,K).*Gamma.*(4.113304+3.114244./curry_func(ny_Q(ny)+ny_Km2(K)));

Int_Rp2 = @(J,K,ny,Gamma) Nrot(J,K).*raman_cross(ny_R(J,ny)+ny_Kp2(K),ny).*Pla_Rp2(J,K).*Gamma.*(4.113304+3.114244./curry_func(ny_R(J,ny)+ny_Kp2(K)));
Int_Rp1 = @(J,K,ny,Gamma) Nrot(J,K).*raman_cross(ny_R(J,ny)+ny_Kp1(K),ny).*Pla_Rp1(J,K).*Gamma.*(4.113304+3.114244./curry_func(ny_R(J,ny)+ny_Kp1(K)));
Int_Rp0 = @(J,K,ny,Gamma) Nrot(J,K).*raman_cross(ny_R(J,ny)+ny_Kp0(K),ny).*Pla_Rp0(J,K).*Gamma.*(4.113304+3.114244./curry_func(ny_R(J,ny)+ny_Kp0(K)));
Int_Rm1 = @(J,K,ny,Gamma) Nrot(J,K).*raman_cross(ny_R(J,ny)+ny_Km1(K),ny).*Pla_Rm1(J,K).*Gamma.*(4.113304+3.114244./curry_func(ny_R(J,ny)+ny_Km1(K)));
Int_Rm2 = @(J,K,ny,Gamma) Nrot(J,K).*raman_cross(ny_R(J,ny)+ny_Km2(K),ny).*Pla_Rm2(J,K).*Gamma.*(4.113304+3.114244./curry_func(ny_R(J,ny)+ny_Km2(K)));

Int_Sp2 = @(J,K,ny,Gamma) Nrot(J,K).*raman_cross(ny_S(J,ny)+ny_Kp2(K),ny).*Pla_Sp2(J,K).*Gamma.*(4.113304+3.114244./curry_func(ny_S(J,ny)+ny_Kp2(K)));
Int_Sp1 = @(J,K,ny,Gamma) Nrot(J,K).*raman_cross(ny_S(J,ny)+ny_Kp1(K),ny).*Pla_Sp1(J,K).*Gamma.*(4.113304+3.114244./curry_func(ny_S(J,ny)+ny_Kp1(K)));
Int_Sp0 = @(J,K,ny,Gamma) Nrot(J,K).*raman_cross(ny_S(J,ny)+ny_Kp0(K),ny).*Pla_Sp0(J,K).*Gamma.*(4.113304+3.114244./curry_func(ny_S(J,ny)+ny_Kp0(K)));
Int_Sm1 = @(J,K,ny,Gamma) Nrot(J,K).*raman_cross(ny_S(J,ny)+ny_Km1(K),ny).*Pla_Sm1(J,K).*Gamma.*(4.113304+3.114244./curry_func(ny_S(J,ny)+ny_Km1(K)));
Int_Sm2 = @(J,K,ny,Gamma) Nrot(J,K).*raman_cross(ny_S(J,ny)+ny_Km2(K),ny).*Pla_Sm2(J,K).*Gamma.*(4.113304+3.114244./curry_func(ny_S(J,ny)+ny_Km2(K)));

% Die gleichen Funktionen für parallele Eingangspolarisation: 3.151072gamma^2 + 3.149239gamma^2/curry_func

Int_Op2_para = @(J,K,ny,Gamma) max(Nrot(J,K).*raman_cross(ny_O(J,ny)+ny_Kp2(K),ny).*Pla_Op2(J,K).*Gamma.*(3.151072+3.149239./curry_func(ny_O(J,ny)+ny_Kp2(K))),0);
Int_Op1_para = @(J,K,ny,Gamma) max(Nrot(J,K).*raman_cross(ny_O(J,ny)+ny_Kp1(K),ny).*Pla_Op1(J,K).*Gamma.*(3.151072+3.149239./curry_func(ny_O(J,ny)+ny_Kp1(K))),0);
Int_Op0_para = @(J,K,ny,Gamma) max(Nrot(J,K).*raman_cross(ny_O(J,ny)+ny_Kp0(K),ny).*Pla_Op0(J,K).*Gamma.*(3.151072+3.149239./curry_func(ny_O(J,ny)+ny_Kp0(K))),0);
Int_Om1_para = @(J,K,ny,Gamma) max(Nrot(J,K).*raman_cross(ny_O(J,ny)+ny_Km1(K),ny).*Pla_Om1(J,K).*Gamma.*(3.151072+3.149239./curry_func(ny_O(J,ny)+ny_Km1(K))),0);
Int_Om2_para = @(J,K,ny,Gamma) max(Nrot(J,K).*raman_cross(ny_O(J,ny)+ny_Km2(K),ny).*Pla_Om2(J,K).*Gamma.*(3.151072+3.149239./curry_func(ny_O(J,ny)+ny_Km2(K))),0);

Int_Pp2_para = @(J,K,ny,Gamma) max(Nrot(J,K).*raman_cross(ny_P(J,ny)+ny_Kp2(K),ny).*Pla_Pp2(J,K).*Gamma.*(3.151072+3.149239./curry_func(ny_P(J,ny)+ny_Kp2(K))),0);
Int_Pp1_para = @(J,K,ny,Gamma) max(Nrot(J,K).*raman_cross(ny_P(J,ny)+ny_Kp1(K),ny).*Pla_Pp1(J,K).*Gamma.*(3.151072+3.149239./curry_func(ny_P(J,ny)+ny_Kp1(K))),0);
Int_Pp0_para = @(J,K,ny,Gamma) max(Nrot(J,K).*raman_cross(ny_P(J,ny)+ny_Kp0(K),ny).*Pla_Pp0(J,K).*Gamma.*(3.151072+3.149239./curry_func(ny_P(J,ny)+ny_Kp0(K))),0);
Int_Pm1_para = @(J,K,ny,Gamma) max(Nrot(J,K).*raman_cross(ny_P(J,ny)+ny_Km1(K),ny).*Pla_Pm1(J,K).*Gamma.*(3.151072+3.149239./curry_func(ny_P(J,ny)+ny_Km1(K))),0);
Int_Pm2_para = @(J,K,ny,Gamma) max(Nrot(J,K).*raman_cross(ny_P(J,ny)+ny_Km2(K),ny).*Pla_Pm2(J,K).*Gamma.*(3.151072+3.149239./curry_func(ny_P(J,ny)+ny_Km2(K))),0);

Int_Qp2_para = @(J,K,ny,Gamma) max(Nrot(J,K).*raman_cross(ny_Q(ny)+ny_Kp2(K),ny).*Pla_Qp2(J,K).*Gamma.*(3.151072+3.149239./curry_func(ny_Q(ny)+ny_Kp2(K))),0);
Int_Qp1_para = @(J,K,ny,Gamma) max(Nrot(J,K).*raman_cross(ny_Q(ny)+ny_Kp1(K),ny).*Pla_Qp1(J,K).*Gamma.*(3.151072+3.149239./curry_func(ny_Q(ny)+ny_Kp1(K))),0);
Int_Qp0_para = @(J,K,ny,Gamma) max(Nrot(J,K).*raman_cross(ny_Q(ny)+ny_Kp0(K),ny).*Pla_Qp0(J,K).*Gamma.*(3.151072+3.149239./curry_func(ny_Q(ny)+ny_Kp0(K))),0);
Int_Qp0_para_alpha = @(ny,Alpha) raman_cross(ny_Q(ny),ny).*Alpha.*(1.699557+1.617074./curry_func(ny));
Int_Qm1_para = @(J,K,ny,Gamma) max(Nrot(J,K).*raman_cross(ny_Q(ny)+ny_Km1(K),ny).*Pla_Qm1(J,K).*Gamma.*(3.151072+3.149239./curry_func(ny_Q(ny)+ny_Km1(K))),0);
Int_Qm2_para = @(J,K,ny,Gamma) max(Nrot(J,K).*raman_cross(ny_Q(ny)+ny_Km2(K),ny).*Pla_Qm2(J,K).*Gamma.*(3.151072+3.149239./curry_func(ny_Q(ny)+ny_Km2(K))),0);

Int_Rp2_para = @(J,K,ny,Gamma) max(Nrot(J,K).*raman_cross(ny_R(J,ny)+ny_Kp2(K),ny).*Pla_Rp2(J,K).*Gamma.*(3.151072+3.149239./curry_func(ny_R(J,ny)+ny_Kp2(K))),0);
Int_Rp1_para = @(J,K,ny,Gamma) max(Nrot(J,K).*raman_cross(ny_R(J,ny)+ny_Kp1(K),ny).*Pla_Rp1(J,K).*Gamma.*(3.151072+3.149239./curry_func(ny_R(J,ny)+ny_Kp1(K))),0);
Int_Rp0_para = @(J,K,ny,Gamma) max(Nrot(J,K).*raman_cross(ny_R(J,ny)+ny_Kp0(K),ny).*Pla_Rp0(J,K).*Gamma.*(3.151072+3.149239./curry_func(ny_R(J,ny)+ny_Kp0(K))),0);
Int_Rm1_para = @(J,K,ny,Gamma) max(Nrot(J,K).*raman_cross(ny_R(J,ny)+ny_Km1(K),ny).*Pla_Rm1(J,K).*Gamma.*(3.151072+3.149239./curry_func(ny_R(J,ny)+ny_Km1(K))),0);
Int_Rm2_para = @(J,K,ny,Gamma) max(Nrot(J,K).*raman_cross(ny_R(J,ny)+ny_Km2(K),ny).*Pla_Rm2(J,K).*Gamma.*(3.151072+3.149239./curry_func(ny_R(J,ny)+ny_Km2(K))),0);

Int_Sp2_para = @(J,K,ny,Gamma) max(Nrot(J,K).*raman_cross(ny_S(J,ny)+ny_Kp2(K),ny).*Pla_Sp2(J,K).*Gamma.*(3.151072+3.149239./curry_func(ny_S(J,ny)+ny_Kp2(K))),0);
Int_Sp1_para = @(J,K,ny,Gamma) max(Nrot(J,K).*raman_cross(ny_S(J,ny)+ny_Kp1(K),ny).*Pla_Sp1(J,K).*Gamma.*(3.151072+3.149239./curry_func(ny_S(J,ny)+ny_Kp1(K))),0);
Int_Sp0_para = @(J,K,ny,Gamma) max(Nrot(J,K).*raman_cross(ny_S(J,ny)+ny_Kp0(K),ny).*Pla_Sp0(J,K).*Gamma.*(3.151072+3.149239./curry_func(ny_S(J,ny)+ny_Kp0(K))),0);
Int_Sm1_para = @(J,K,ny,Gamma) max(Nrot(J,K).*raman_cross(ny_S(J,ny)+ny_Km1(K),ny).*Pla_Sm1(J,K).*Gamma.*(3.151072+3.149239./curry_func(ny_S(J,ny)+ny_Km1(K))),0);
Int_Sm2_para = @(J,K,ny,Gamma) max(Nrot(J,K).*raman_cross(ny_S(J,ny)+ny_Km2(K),ny).*Pla_Sm2(J,K).*Gamma.*(3.151072+3.149239./curry_func(ny_S(J,ny)+ny_Km2(K))),0);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Vorarbeit ist erledigt, jetzt kann über alle Zeilen in der Inputdatei (d.h. über alle Schwingungen) iteriert werden.


for ii = 1:size(Vibrations,1);
  ny = Vibrations(ii,1);
  Alpha = alpha_sq(Vibrations(ii,2),Vibrations(ii,3),Vibrations(ii,4));
  Gamma0 = gamma_sq_dK0(Vibrations(ii,2),Vibrations(ii,3),Vibrations(ii,4));
  Gamma1 = gamma_sq_dK1(Vibrations(ii,6),Vibrations(ii,7));
  Gamma2 = gamma_sq_dK2(Vibrations(ii,2),Vibrations(ii,3),Vibrations(ii,4),Vibrations(ii,5));

%  [ny, sqrt(Gamma0+Gamma1+Gamma2)]

  % Die Intensitäten aller Übergänge werden als Matrix geschrieben. Nach unten steigt J an, nach rechts steigt K an. Für dK=0 werden die Linien eines Js über alle K jeweils aufsummiert (d.h. für jede Zeile alle Spalten addiert, Ergebnis ist ein Spaltenvektor), weil sie die gleiche Wellenzahl haben. Für die Q-Zweige wird für jedes K über alle J aufsummiert (d.h. für jede Spalte über alle Zeilen addiert, Ergebnis ist ein Zeilenvektor, daher transponiert zu Spaltenvektor). Für Qp0 werden alle Linien aufsummiert.
  % Außerdem gelten die Summationsregeln: Im echten symm. Kreisel sind für eine Schwingung je nach Symmetrie entweder dK=±2, ±1 oder 0 erlaubt. Die Summe aller möglichen Anregungen aus einem Zustand heraus muss 1 ergeben. Die Placzek-Faktoren erfüllen diese Regel jedoch nicht vollständig – die Summe über ein einzelnes dK (mit Vorzeichen) ergibt bereits 1, folglich ergeben beide Vorzeichen in Summe 2. Das wird normiert, indem die Zweige mit dk=±1 und ±2 durch 2 geteilt werden.
  % Für Zustände ab K=2 ist das Verfahren gleich, jedoch stellen K=1 und K=0 Sonderfälle dar. Aus K=0 sind nur Übergänge mit positivem dK möglich, sodass keine Normierung notwendig ist. Für K=1 müssen die Zweige mit dK=±1 durch 2 geteilt werden, der Zweig mit dK=+2 wieder nicht.
  % Zunächst folgt das generelle Verfahren für Zustände ab K=2, danach kommen die Sonderfälle.
  % Ausnahme: Der Qp0-Zweig wird vollständig abgefrühstückt, weil da alle Linien zusammenaddiert werden können.

  Branch_Op2 = [Int_Op2([0:Jmax]',[2:Kmax],ny,Gamma2)]/2;
 Branch_Op2_para = [Int_Op2_para([0:Jmax]',[2:Kmax],ny,Gamma2)]/2;
  Branch_Op1 = [Int_Op1([0:Jmax]',[2:Kmax],ny,Gamma1)]/2;
 Branch_Op1_para = [Int_Op1_para([0:Jmax]',[2:Kmax],ny,Gamma1)]/2;
  Branch_Op0 = [sum(Int_Op0([0:Jmax]',[2:Kmax],ny,Gamma0),2)];
 Branch_Op0_para = [sum(Int_Op0_para([0:Jmax]',[2:Kmax],ny,Gamma0),2)];
  Branch_Om1 = [Int_Om1([0:Jmax]',[2:Kmax],ny,Gamma1)]/2;
 Branch_Om1_para = [Int_Om1_para([0:Jmax]',[2:Kmax],ny,Gamma1)]/2;
  Branch_Om2 = [Int_Om2([0:Jmax]',[2:Kmax],ny,Gamma2)]/2;
 Branch_Om2_para = [Int_Om2_para([0:Jmax]',[2:Kmax],ny,Gamma2)]/2;

  Branch_Pp2 = [Int_Pp2([0:Jmax]',[2:Kmax],ny,Gamma2)]/2;
 Branch_Pp2_para = [Int_Pp2_para([0:Jmax]',[2:Kmax],ny,Gamma2)]/2;
  Branch_Pp1 = [Int_Pp1([0:Jmax]',[2:Kmax],ny,Gamma1)]/2;
 Branch_Pp1_para = [Int_Pp1_para([0:Jmax]',[2:Kmax],ny,Gamma1)]/2;
  Branch_Pp0 = [sum(Int_Pp0([0:Jmax]',[2:Kmax],ny,Gamma0),2)];
 Branch_Pp0_para = [sum(Int_Pp0_para([0:Jmax]',[2:Kmax],ny,Gamma0),2)];
  Branch_Pm1 = [Int_Pm1([0:Jmax]',[2:Kmax],ny,Gamma1)]/2;
 Branch_Pm1_para = [Int_Pm1_para([0:Jmax]',[2:Kmax],ny,Gamma1)]/2;
  Branch_Pm2 = [Int_Pm2([0:Jmax]',[2:Kmax],ny,Gamma2)]/2;
 Branch_Pm2_para = [Int_Pm2_para([0:Jmax]',[2:Kmax],ny,Gamma2)]/2;

  Branch_Qp2 = [sum(Int_Qp2([0:Jmax]',[2:Kmax],ny,Gamma2),1)']/2;
 Branch_Qp2_para = [sum(Int_Qp2_para([0:Jmax]',[2:Kmax],ny,Gamma2),1)']/2;
  Branch_Qp1 = [sum(Int_Qp1([0:Jmax]',[2:Kmax],ny,Gamma1),1)']/2;
 Branch_Qp1_para = [sum(Int_Qp1_para([0:Jmax]',[2:Kmax],ny,Gamma1),1)']/2;
  Branch_Qp0 = [sum(Int_Qp0([0:Jmax]',[0:Kmax],ny,Gamma0)(:))];
 Branch_Qp0_para = [sum(Int_Qp0_para([0:Jmax]',[0:Kmax],ny,Gamma0)(:))];
  Branch_Qm1 = [sum(Int_Qm1([0:Jmax]',[2:Kmax],ny,Gamma1),1)']/2;
 Branch_Qm1_para = [sum(Int_Qm1_para([0:Jmax]',[2:Kmax],ny,Gamma1),1)']/2;
  Branch_Qm2 = [sum(Int_Qm2([0:Jmax]',[2:Kmax],ny,Gamma2),1)']/2;
 Branch_Qm2_para = [sum(Int_Qm2_para([0:Jmax]',[2:Kmax],ny,Gamma2),1)']/2;

  Branch_Rp2 = [Int_Rp2([0:Jmax]',[2:Kmax],ny,Gamma2)]/2;
 Branch_Rp2_para = [Int_Rp2_para([0:Jmax]',[2:Kmax],ny,Gamma2)]/2;
  Branch_Rp1 = [Int_Rp1([0:Jmax]',[2:Kmax],ny,Gamma1)]/2;
 Branch_Rp1_para = [Int_Rp1_para([0:Jmax]',[2:Kmax],ny,Gamma1)]/2;
  Branch_Rp0 = [sum(Int_Rp0([0:Jmax]',[2:Kmax],ny,Gamma0),2)];
 Branch_Rp0_para = [sum(Int_Rp0_para([0:Jmax]',[2:Kmax],ny,Gamma0),2)];
  Branch_Rm1 = [Int_Rm1([0:Jmax]',[2:Kmax],ny,Gamma1)]/2;
 Branch_Rm1_para = [Int_Rm1_para([0:Jmax]',[2:Kmax],ny,Gamma1)]/2;
  Branch_Rm2 = [Int_Rm2([0:Jmax]',[2:Kmax],ny,Gamma2)]/2;
 Branch_Rm2_para = [Int_Rm2_para([0:Jmax]',[2:Kmax],ny,Gamma2)]/2;

  Branch_Sp2 = [Int_Sp2([0:Jmax]',[2:Kmax],ny,Gamma2)]/2;
 Branch_Sp2_para = [Int_Sp2_para([0:Jmax]',[2:Kmax],ny,Gamma2)]/2;
  Branch_Sp1 = [Int_Sp1([0:Jmax]',[2:Kmax],ny,Gamma1)]/2;
 Branch_Sp1_para = [Int_Sp1_para([0:Jmax]',[2:Kmax],ny,Gamma1)]/2;
  Branch_Sp0 = [sum(Int_Sp0([0:Jmax]',[2:Kmax],ny,Gamma0),2)];
 Branch_Sp0_para = [sum(Int_Sp0_para([0:Jmax]',[2:Kmax],ny,Gamma0),2)];
  Branch_Sm1 = [Int_Sm1([0:Jmax]',[2:Kmax],ny,Gamma1)]/2;
 Branch_Sm1_para = [Int_Sm1_para([0:Jmax]',[2:Kmax],ny,Gamma1)]/2;
  Branch_Sm2 = [Int_Sm2([0:Jmax]',[2:Kmax],ny,Gamma2)]/2;
 Branch_Sm2_para = [Int_Sm2_para([0:Jmax]',[2:Kmax],ny,Gamma2)]/2;

  % Dazugehörige Wellenzahlen

  ny_Op2 = ny_O([0:Jmax]',ny)+ny_Kp2([2:Kmax]);
  ny_Op1 = ny_O([0:Jmax]',ny)+ny_Kp1([2:Kmax]);
  ny_Op0 = ny_O([0:Jmax]',ny);
  ny_Om1 = ny_O([0:Jmax]',ny)+ny_Km1([2:Kmax]);
  ny_Om2 = ny_O([0:Jmax]',ny)+ny_Km2([2:Kmax]);

  ny_Pp2 = ny_P([0:Jmax]',ny)+ny_Kp2([2:Kmax]);
  ny_Pp1 = ny_P([0:Jmax]',ny)+ny_Kp1([2:Kmax]);
  ny_Pp0 = ny_P([0:Jmax]',ny);
  ny_Pm1 = ny_P([0:Jmax]',ny)+ny_Km1([2:Kmax]);
  ny_Pm2 = ny_P([0:Jmax]',ny)+ny_Km2([2:Kmax]);

  ny_Qp2 = ny_Q(ny)+ny_Kp2([2:Kmax]');
  ny_Qp1 = ny_Q(ny)+ny_Kp1([2:Kmax]');
  ny_Qp0 = ny_Q(ny);
  ny_Qm1 = ny_Q(ny)+ny_Km1([2:Kmax]');
  ny_Qm2 = ny_Q(ny)+ny_Km2([2:Kmax]');

  ny_Rp2 = ny_R([0:Jmax]',ny)+ny_Kp2([2:Kmax]);
  ny_Rp1 = ny_R([0:Jmax]',ny)+ny_Kp1([2:Kmax]);
  ny_Rp0 = ny_R([0:Jmax]',ny);
  ny_Rm1 = ny_R([0:Jmax]',ny)+ny_Km1([2:Kmax]);
  ny_Rm2 = ny_R([0:Jmax]',ny)+ny_Km2([2:Kmax]);

  ny_Sp2 = ny_S([0:Jmax]',ny)+ny_Kp2([2:Kmax]);
  ny_Sp1 = ny_S([0:Jmax]',ny)+ny_Kp1([2:Kmax]);
  ny_Sp0 = ny_S([0:Jmax]',ny);
  ny_Sm1 = ny_S([0:Jmax]',ny)+ny_Km1([2:Kmax]);
  ny_Sm2 = ny_S([0:Jmax]',ny)+ny_Km2([2:Kmax]);

  % Jetzt die Zustände mit K=1, hier müssen nur die Übergänge mit dK=±1 normiert werden. Die Summation über K bei dK=0 wie sie oben durchgeführt wurde entfällt hier naturgemäß. Übergänge mit dK=-2 entfallen. Q-Zweige wie oben über alle J summiert. Qp0-Zweig wurde bereits abgehandelt.

  Branch_Op2_K1 = [Int_Op2([0:Jmax]',1,ny,Gamma2)];
 Branch_Op2_para_K1 = [Int_Op2_para([0:Jmax]',1,ny,Gamma2)];
  Branch_Op1_K1 = [Int_Op1([0:Jmax]',1,ny,Gamma1)]/2;
 Branch_Op1_para_K1 = [Int_Op1_para([0:Jmax]',1,ny,Gamma1)]/2;
  Branch_Op0_K1 = [Int_Op0([0:Jmax]',1,ny,Gamma0)];
 Branch_Op0_para_K1 = [Int_Op0_para([0:Jmax]',1,ny,Gamma0)];
  Branch_Om1_K1 = [Int_Om1([0:Jmax]',1,ny,Gamma1)]/2;
 Branch_Om1_para_K1 = [Int_Om1_para([0:Jmax]',1,ny,Gamma1)]/2;

  Branch_Pp2_K1 = [Int_Pp2([0:Jmax]',1,ny,Gamma2)];
 Branch_Pp2_para_K1 = [Int_Pp2_para([0:Jmax]',1,ny,Gamma2)];
  Branch_Pp1_K1 = [Int_Pp1([0:Jmax]',1,ny,Gamma1)]/2;
 Branch_Pp1_para_K1 = [Int_Pp1_para([0:Jmax]',1,ny,Gamma1)]/2;
  Branch_Pp0_K1 = [Int_Pp0([0:Jmax]',1,ny,Gamma0)];
 Branch_Pp0_para_K1 = [Int_Pp0_para([0:Jmax]',1,ny,Gamma0)];
  Branch_Pm1_K1 = [Int_Pm1([0:Jmax]',1,ny,Gamma1)]/2;
 Branch_Pm1_para_K1 = [Int_Pm1_para([0:Jmax]',1,ny,Gamma1)]/2;

  Branch_Qp2_K1 = sum([Int_Qp2([0:Jmax]',1,ny,Gamma2)],1);
 Branch_Qp2_para_K1 = sum([Int_Qp2_para([0:Jmax]',1,ny,Gamma2)],1);
  Branch_Qp1_K1 = sum([Int_Qp1([0:Jmax]',1,ny,Gamma1)],1)/2;
 Branch_Qp1_para_K1 = sum([Int_Qp1_para([0:Jmax]',1,ny,Gamma1)],1)/2;
  Branch_Qm1_K1 = sum([Int_Qm1([0:Jmax]',1,ny,Gamma1)],1)/2;
 Branch_Qm1_para_K1 = sum([Int_Qm1_para([0:Jmax]',1,ny,Gamma1)],1)/2;

  Branch_Rp2_K1 = [Int_Rp2([0:Jmax]',1,ny,Gamma2)];
 Branch_Rp2_para_K1 = [Int_Rp2_para([0:Jmax]',1,ny,Gamma2)];
  Branch_Rp1_K1 = [Int_Rp1([0:Jmax]',1,ny,Gamma1)]/2;
 Branch_Rp1_para_K1 = [Int_Rp1_para([0:Jmax]',1,ny,Gamma1)]/2;
  Branch_Rp0_K1 = [Int_Rp0([0:Jmax]',1,ny,Gamma0)];
 Branch_Rp0_para_K1 = [Int_Rp0_para([0:Jmax]',1,ny,Gamma0)];
  Branch_Rm1_K1 = [Int_Rm1([0:Jmax]',1,ny,Gamma1)]/2;
 Branch_Rm1_para_K1 = [Int_Rm1_para([0:Jmax]',1,ny,Gamma1)]/2;

  Branch_Sp2_K1 = [Int_Sp2([0:Jmax]',1,ny,Gamma2)];
 Branch_Sp2_para_K1 = [Int_Sp2_para([0:Jmax]',1,ny,Gamma2)];
  Branch_Sp1_K1 = [Int_Sp1([0:Jmax]',1,ny,Gamma1)]/2;
 Branch_Sp1_para_K1 = [Int_Sp1_para([0:Jmax]',1,ny,Gamma1)]/2;
  Branch_Sp0_K1 = [Int_Sp0([0:Jmax]',1,ny,Gamma0)];
 Branch_Sp0_para_K1 = [Int_Sp0_para([0:Jmax]',1,ny,Gamma0)];
  Branch_Sm1_K1 = [Int_Sm1([0:Jmax]',1,ny,Gamma1)]/2;
 Branch_Sm1_para_K1 = [Int_Sm1_para([0:Jmax]',1,ny,Gamma1)]/2;

  % Erneut die entsprechenden Wellenzahlen der Übergänge

  ny_Op2_K1 = ny_O([0:Jmax]',ny)+ny_Kp2(1);
  ny_Op1_K1 = ny_O([0:Jmax]',ny)+ny_Kp1(1);
  ny_Op0_K1 = ny_O([0:Jmax]',ny)+ny_Kp0(1);
  ny_Om1_K1 = ny_O([0:Jmax]',ny)+ny_Km1(1);

  ny_Pp2_K1 = ny_P([0:Jmax]',ny)+ny_Kp2(1);
  ny_Pp1_K1 = ny_P([0:Jmax]',ny)+ny_Kp1(1);
  ny_Pp0_K1 = ny_P([0:Jmax]',ny)+ny_Kp0(1);
  ny_Pm1_K1 = ny_P([0:Jmax]',ny)+ny_Km1(1);

  ny_Qp2_K1 = ny_Q(ny)+ny_Kp2(1);
  ny_Qp1_K1 = ny_Q(ny)+ny_Kp1(1);
  ny_Qm1_K1 = ny_Q(ny)+ny_Km1(1);

  ny_Rp2_K1 = ny_R([0:Jmax]',ny)+ny_Kp2(1);
  ny_Rp1_K1 = ny_R([0:Jmax]',ny)+ny_Kp1(1);
  ny_Rp0_K1 = ny_R([0:Jmax]',ny)+ny_Kp0(1);
  ny_Rm1_K1 = ny_R([0:Jmax]',ny)+ny_Km1(1);

  ny_Sp2_K1 = ny_S([0:Jmax]',ny)+ny_Kp2(1);
  ny_Sp1_K1 = ny_S([0:Jmax]',ny)+ny_Kp1(1);
  ny_Sp0_K1 = ny_S([0:Jmax]',ny)+ny_Kp0(1);
  ny_Sm1_K1 = ny_S([0:Jmax]',ny)+ny_Km1(1);

  % Übergänge aus K=0, hier muss nichts mehr durch 2 geteilt werden. Übergänge mit dK=-1 und -2 entfallen.

  Branch_Op2_K0 = [Int_Op2([0:Jmax]',0,ny,Gamma2)];
 Branch_Op2_para_K0 = [Int_Op2_para([0:Jmax]',0,ny,Gamma2)];
  Branch_Op1_K0 = [Int_Op1([0:Jmax]',0,ny,Gamma1)];
 Branch_Op1_para_K0 = [Int_Op1_para([0:Jmax]',0,ny,Gamma1)];
  Branch_Op0_K0 = [Int_Op0([0:Jmax]',0,ny,Gamma0)];
 Branch_Op0_para_K0 = [Int_Op0_para([0:Jmax]',0,ny,Gamma0)];

  Branch_Pp2_K0 = [Int_Pp2([0:Jmax]',0,ny,Gamma2)];
 Branch_Pp2_para_K0 = [Int_Pp2_para([0:Jmax]',0,ny,Gamma2)];
  Branch_Pp1_K0 = [Int_Pp1([0:Jmax]',0,ny,Gamma1)];
 Branch_Pp1_para_K0 = [Int_Pp1_para([0:Jmax]',0,ny,Gamma1)];
 % Pla_Pp0(K=0) ist immer 0

  Branch_Qp2_K0 = sum([Int_Qp2([0:Jmax]',0,ny,Gamma2)]);
 Branch_Qp2_para_K0 = sum([Int_Qp2_para([0:Jmax]',0,ny,Gamma2)]);
  Branch_Qp1_K0 = sum([Int_Qp1([0:Jmax]',0,ny,Gamma1)]);
 Branch_Qp1_para_K0 = sum([Int_Qp1_para([0:Jmax]',0,ny,Gamma1)]);

  Branch_Rp2_K0 = [Int_Rp2([0:Jmax]',0,ny,Gamma2)];
 Branch_Rp2_para_K0 = [Int_Rp2_para([0:Jmax]',0,ny,Gamma2)];
  Branch_Rp1_K0 = [Int_Rp1([0:Jmax]',0,ny,Gamma1)];
 Branch_Rp1_para_K0 = [Int_Rp1_para([0:Jmax]',0,ny,Gamma1)];
 % Pla_Rp0(K=0) ist immer 0

  Branch_Sp2_K0 = [Int_Sp2([0:Jmax]',0,ny,Gamma2)];
 Branch_Sp2_para_K0 = [Int_Sp2_para([0:Jmax]',0,ny,Gamma2)];
  Branch_Sp1_K0 = [Int_Sp1([0:Jmax]',0,ny,Gamma1)];
 Branch_Sp1_para_K0 = [Int_Sp1_para([0:Jmax]',0,ny,Gamma1)];
  Branch_Sp0_K0 = [Int_Sp0([0:Jmax]',0,ny,Gamma0)];
 Branch_Sp0_para_K0 = [Int_Sp0_para([0:Jmax]',0,ny,Gamma0)];

  % Wellenzahlen der Übergänge aus K=0

  ny_Op2_K0 = ny_O([0:Jmax]',ny)+ny_Kp2(0);
  ny_Op1_K0 = ny_O([0:Jmax]',ny)+ny_Kp1(0);
  ny_Op0_K0 = ny_O([0:Jmax]',ny)+ny_Kp0(0);

  ny_Pp2_K0 = ny_P([0:Jmax]',ny)+ny_Kp2(0);
  ny_Pp1_K0 = ny_P([0:Jmax]',ny)+ny_Kp1(0);

  ny_Qp2_K0 = ny_Q(ny)+ny_Kp2(0);
  ny_Qp1_K0 = ny_Q(ny)+ny_Kp1(0);

  ny_Rp2_K0 = ny_R([0:Jmax]',ny)+ny_Kp2(0);
  ny_Rp1_K0 = ny_R([0:Jmax]',ny)+ny_Kp1(0);

  ny_Sp2_K0 = ny_S([0:Jmax]',ny)+ny_Kp2(0);
  ny_Sp1_K0 = ny_S([0:Jmax]',ny)+ny_Kp1(0);
  ny_Sp0_K0 = ny_S([0:Jmax]',ny)+ny_Kp0(0);

  % Alle Linien eines K-Zweiges werden untereinander mit ihrer dazugehörigen Wellenzahl angeordnet. Die Indizierung (:) setzt die Spalten untereinander

  Br_Op2 = [ny_Op2_K0, Branch_Op2_K0, Branch_Op2_para_K0; ny_Op2_K1, Branch_Op2_K1, Branch_Op2_para_K1; ny_Op2(:), Branch_Op2(:), Branch_Op2_para(:)];
  Br_Op1 = [ny_Op1_K0, Branch_Op1_K0, Branch_Op1_para_K0; ny_Op1_K1, Branch_Op1_K1, Branch_Op1_para_K1; ny_Op1(:), Branch_Op1(:), Branch_Op1_para(:)];
  Br_Op0 = [ny_Op0_K0, Branch_Op0_K0, Branch_Op0_para_K0; ny_Op0_K1, Branch_Op0_K1, Branch_Op0_para_K1; ny_Op0(:), Branch_Op0(:), Branch_Op0_para(:)];
  Br_Om1 = [ny_Om1_K1, Branch_Om1_K1, Branch_Om1_para_K1; ny_Om1(:), Branch_Om1(:), Branch_Om1_para(:)];
  Br_Om2 = [ny_Om2(:), Branch_Om2(:), Branch_Om2_para(:)];

  Br_Pp2 = [ny_Pp2_K0, Branch_Pp2_K0, Branch_Pp2_para_K0; ny_Pp2_K1, Branch_Pp2_K1, Branch_Pp2_para_K1; ny_Pp2(:), Branch_Pp2(:), Branch_Pp2_para(:)];
  Br_Pp1 = [ny_Pp1_K0, Branch_Pp1_K0, Branch_Pp1_para_K0; ny_Pp1_K1, Branch_Pp1_K1, Branch_Pp1_para_K1; ny_Pp1(:), Branch_Pp1(:), Branch_Pp1_para(:)];
  Br_Pp0 = [ny_Pp0_K1, Branch_Pp0_K1, Branch_Pp0_para_K1; ny_Pp0(:), Branch_Pp0(:), Branch_Pp0_para(:)];
  Br_Pm1 = [ny_Pm1_K1, Branch_Pm1_K1, Branch_Pm1_para_K1; ny_Pm1(:), Branch_Pm1(:), Branch_Pm1_para(:)];
  Br_Pm2 = [ny_Pm2(:), Branch_Pm2(:), Branch_Pm2_para(:)];

  Br_Qp2 = [ny_Qp2_K0, Branch_Qp2_K0, Branch_Qp2_para_K0; ny_Qp2_K1, Branch_Qp2_K1, Branch_Qp2_para_K1; ny_Qp2(:), Branch_Qp2(:), Branch_Qp2_para(:)];
  Br_Qp1 = [ny_Qp1_K0, Branch_Qp1_K0, Branch_Qp1_para_K0; ny_Qp1_K1, Branch_Qp1_K1, Branch_Qp1_para_K1; ny_Qp1(:), Branch_Qp1(:), Branch_Qp1_para(:)];
  Br_Qp0 = [ny_Qp0(:), Branch_Qp0(:), Branch_Qp0_para(:)];
  Br_Qm1 = [ny_Qm1_K1, Branch_Qm1_K1, Branch_Qm1_para_K1; ny_Qm1(:), Branch_Qm1(:), Branch_Qm1_para(:)];
  Br_Qm2 = [ny_Qm2(:), Branch_Qm2(:), Branch_Qm2_para(:)];

  Br_Rp2 = [ny_Rp2_K0, Branch_Rp2_K0, Branch_Rp2_para_K0; ny_Rp2_K1, Branch_Rp2_K1, Branch_Rp2_para_K1; ny_Rp2(:), Branch_Rp2(:), Branch_Rp2_para(:)];
  Br_Rp1 = [ny_Rp1_K0, Branch_Rp1_K0, Branch_Rp1_para_K0; ny_Rp1_K1, Branch_Rp1_K1, Branch_Rp1_para_K1; ny_Rp1(:), Branch_Rp1(:), Branch_Rp1_para(:)];
  Br_Rp0 = [ny_Rp0_K1, Branch_Rp0_K1, Branch_Rp0_para_K1; ny_Rp0(:), Branch_Rp0(:), Branch_Rp0_para(:)];
  Br_Rm1 = [ny_Rm1_K1, Branch_Rm1_K1, Branch_Rm1_para_K1; ny_Rm1(:), Branch_Rm1(:), Branch_Rm1_para(:)];
  Br_Rm2 = [ny_Rm2(:), Branch_Rm2(:), Branch_Rm2_para(:)];

  Br_Sp2 = [ny_Sp2_K0, Branch_Sp2_K0, Branch_Sp2_para_K0; ny_Sp2_K1, Branch_Sp2_K1, Branch_Sp2_para_K1; ny_Sp2(:), Branch_Sp2(:), Branch_Sp2_para(:)];
  Br_Sp1 = [ny_Sp1_K0, Branch_Sp1_K0, Branch_Sp1_para_K0; ny_Sp1_K1, Branch_Sp1_K1, Branch_Sp1_para_K1; ny_Sp1(:), Branch_Sp1(:), Branch_Sp1_para(:)];
  Br_Sp0 = [ny_Sp0_K0, Branch_Sp0_K0, Branch_Sp0_para_K0; ny_Sp0_K1, Branch_Sp0_K1, Branch_Sp0_para_K1; ny_Sp0(:), Branch_Sp0(:), Branch_Sp0_para(:)];
  Br_Sm1 = [ny_Sm1_K1, Branch_Sm1_K1, Branch_Sm1_para_K1; ny_Sm1(:), Branch_Sm1(:), Branch_Sm1_para(:)];
  Br_Sm2 = [ny_Sm2(:), Branch_Sm2(:), Branch_Sm2_para(:)];

  % Alle Zweige untereinander in einer 3xN-Matrix, gespeichert als Eintrag in einer Zelle

  % Zunächst dK=±2
  Br_all_dK2{ii} = [Br_Op2; Br_Om2; ...
  Br_Pp2; Br_Pm2; ...
  Br_Qp2; Br_Qm2; ...
  Br_Rp2; Br_Rm2; ...
  Br_Sp2; Br_Sm2];

  % dK=±1
  Br_all_dK1{ii} = [Br_Op1; Br_Om1; ...
  Br_Pp1; Br_Pm1; ...
  Br_Qp1; Br_Qm1; ...
  Br_Rp1; Br_Rm1; ...
  Br_Sp1; Br_Sm1];

  % dK=0
  Br_all_dK0{ii} = [Br_Op0; ...
  Br_Pp0; ...
  Br_Qp0; ...
  Br_Rp0; ...
  Br_Sp0];

  % der Alpha-Anteil am Qp0-Zweig wurde vorhin ausgelassen, das wird jetzt nachgeholt. Für jede Schwingung ist das entsprechend eine einzige Zeile. Weil die Summe über alle Rotationszustände 1 ergibt, muss hierbei nicht nochmal explizit summiert werden. In der dritten Spalte steht die Intensität bei parallel polarisierter Eingangsstrahlung, wegen Integration über Raumwinkel ungleich 0.

  Br_all_alpha{ii} = [ny_Q(ny), Int_Qp0_alpha(ny,Alpha), Int_Qp0_para_alpha(ny,Alpha)];

%  % Schwingungen in einzelne Dateien schreiben
%  dlmwrite(["ny_" num2str(size(Vibrations,1)-ii+1) ".dat"], [Br_all_dK2{ii}; Br_all_dK1{ii}; Br_all_dK0{ii}; Br_all_alpha{ii}], "\t");

% Ende der Schleife über alle Schwingungen
  endfor

% Die Cellarrays Br_all werden in eine Matrix überführt

Br_all_dK2 = cell2mat(Br_all_dK2');
Br_all_dK1 = cell2mat(Br_all_dK1');
Br_all_dK0 = cell2mat(Br_all_dK0');
Br_all_alpha = cell2mat(Br_all_alpha');

% Die Beiträge aus den unterschiedlichen dK-Zweigen werden zusammengetan.

Br_all = [Br_all_dK2; ...
Br_all_dK1; ...
Br_all_dK0; ...
Br_all_alpha];

% Einzelne Linien aus erlaubten Übergängen haben eine Intensität von ungefähr 1e-40 (die Summe landet bei ca. 1e-34), während verbotene/inaktive eher so bei 1e-55 - 1e-70 liegen. Daher werden alle <1e-50 gelöscht, um unnötige Linien zu vermeiden.

cutoff=1e-50;
Br_all(Br_all(:,2)<=cutoff,:)=[];

# Alle Linien aufsteigend sortieren
Br_all = sortrows(Br_all,1);

%Checksum=cell2mat(Checksum_cell')

% Sofern nicht ausgeschaltet, wird auf 0.01cm-1 gerundet und dann jeweils aufsummiert, um die Gesamtmenge an Linien zu reduzieren
if p.Results.dont_round == 0
  % Zunächst Wellenzahl runden
  precision=0.01;
  Br_all_round = [round(Br_all(:,1)/precision)*precision, Br_all(:,2:3)];
  % einzigartige Wellenzahlen finden. unique_wn enthält die Wellenzahlen, identical_indices ist ein Vektor mit der gleichen Länge wie Br_all, der die Zuordnung der Einträge zu den Wellenzahlen beinhaltet. Sind z.B. die ersten drei Wellenzahlen in Br_all [10.00, 10.01, 10.01, ...], dann steht in identical_indices [1, 2, 2, ...].
  [unique_wn,~,identical_indices] = unique(Br_all_round(:,1));
  % Einträge mit identischer Wellenzahl summieren
  Br_all = [unique_wn, accumarray(identical_indices, Br_all_round(:,2)), accumarray(identical_indices, Br_all_round(:,3)),];
endif

% Wenn angefordert, werden die einzelnen Linien als solche gespeichert und dann beendet, ohne Gaußglättung
if p.Results.print_lines
  dlmwrite(["Vibrot_" num2str(Trot) "K_raw.dat"], Br_all, "\t");
  return  %Rest wird nicht mehr ausgeführt
endif

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Gaußverbreiterung/-glättung.

%% Auflösung von 0,1cm-1
%% Für Ester ist alles >2000cm-1 uninteressant
xmin=0;
xmax=2000;
res=0.1;
x = linspace (xmin, xmax, (xmax-xmin)/res+1);

%% Intensität zunächst als leerer Zeilenvektor angelegt
outdata_ortho = zeros(1,length(x));
outdata_para = zeros(1,length(x));

% Beschneiden der Daten, alle Linien außerhalb des Samplingbereichs werden weggeschnitten
Br_all(Br_all(:,1)>max(x),:)=[];

%%% 2. Versuch der Gaußglättung
function retval = gauss(x,x0,I,sigma)
retval = I./(sigma.*sqrt(2*pi)).*exp(-0.5*((x-x0)./sigma).^2);
endfunction
% sig = 1.0; wird beim Input definiert
sig = p.Results.sig;

% Gaußkurve soll nur innerhalb ±5sigma berechnet werden, und nur in dem entsprechenden Bereich zum Ergebnisvektor addiert werden, d.h. es werden keine 0en woanders addiert.
% Mir ist noch keine gute Möglichkeit zum Vektorisieren/Parallelisieren eingefallen, daher individuell iterativ über alle Rotationslinien.
for i = 1:length(Br_all)
  % Vektor (genauso lang wie x) mit 0 und 1en, der anzeigt, wo die Gaußkurve berechnet werden soll.
  sampling_space=(x>=(Br_all(i,1)-5*sig) & x<=(Br_all(i,1)+5*sig));
  % Gaußkurve für 90° und 0° wird in diesem Bereich berechnet
  gauss_ortho = gauss(x(sampling_space), Br_all(i,1), Br_all(i,2), sig);
  gauss_para  = gauss(x(sampling_space), Br_all(i,1), Br_all(i,3), sig);
  % jeweils zum Ergebnisvektor an der richtigen Stelle addiert
  outdata_ortho(sampling_space) += gauss_ortho;
  outdata_para(sampling_space)  += gauss_para;
endfor

%% Transponieren der Daten
exportdata = [x', outdata_ortho', outdata_para'];

%% Speichern
dlmwrite(["Vibrot_" num2str(Trot) "K_" num2str(sig) "cm.dat"], exportdata, "\t");

% Ausgabe der Zeitstatistik, wird von Slurm aufgefangen
toc

endfunction
