#!/bin/bash

# Wenn keine control-Datei vorhanden, wird abgebrochen.

if [ ! -f control ]; then
	echo "No control found, aborting..."
	echo "Run this script in a folder with TURBOMOLE Raman calculation results!"
	exit 1
fi

# Die Rotationskonstanten werden in eine separate Datei geschrieben, dafür wird das vom TURBOMOLE-Support zur Verfügung gestellte Tool rotconst verwendet.
rotconst_mainmass | awk '/rotational constants in  cm/{getline; print}' > rot_constants.out

# Die Schwingungswellenzahlen werden aus control ausgelesen
awk '/raman spectrum/{flag=1} /the polarizability anisotropy is commonly referred/{flag=0} flag' control | tail -n+13 | tr -s ' '| cut -d" " -f4 > wavenumbers.out

# Die Normalmoden in vib_normal_modes sind nicht brauchbar, da sie noch Rot + Trans beinhalten. Die korrekt projizierten müssen von tm2molden erzeugt werden.
# tm2molden fragt interaktiv, was in die Datei geschrieben werden soll, hier wird die Tastatureingabe als stdin eingegeben: Die geschriebene Datei heißt molden.tmp, es sollen keine MOs geschrieben werden (erstes n), die Frequenzen sollen geschrieben werden (y), keine Strukturoptimierungsinfo (zweites n). Immer jeweils getrennt durch Zeilenumbruch \n.
# Falls molden.tmp bereits aus einem vorherigen Aufruf vorhanden war, wird diese gelöscht.
if [ -f molden.tmp ]; then rm molden.tmp; fi

module load Turbomole
echo $'molden.tmp\nn\ny\nn\n' | tm2molden > /dev/null

# Die Normalmoden werden von einem perl-Skript so umsortiert, dass alle Koordinaten (x1, y1, z1, x2, y2, z2...) hintereinander in einer Zeile stehen. Die Schwingungen stehen dann untereinander. Um am Ende der letzten Zeile noch einen Zeilenumbruch einzufügen, wird paste aufgerufen (somit gibt 'wc -l vibmodes.out' wieder die korrekte Anzahl Schwingungen aus).
perl $HOME/bin/vib-extract-perl molden.tmp | paste > vibmodes.out

# Die Schwingungen sind mit reduzierten Massen normiert, das muss rückgängig gemacht werden. (gilt auch für molden-Input)
# Die Massen stehen in control, awk gibt alle Zeilen zw. "vibrational reduced masses" und "nvibro" aus, paste entfernt alle Zeilenumbrüche bis auf den letzten. Am Ende gibt es eine Zeile mit den Einträgen µ(vib1) µ(vib2) µ(vib3)...
awk '/vibrational reduced masses/{flag=1;next} /nvibro/{flag=0} flag' control | paste -s -d ' ' > reduced_masses.out

# Die 6 Komponenten des Polarisierbarkeitstensors werden als jeweils eine Zeile nach dxyz.out geschrieben. Exponentenzeichen D wird durch E ersetzt, damit Octave das versteht. (der erste Aufruf überschreibt eine eventuell vorhandene Datei, die restlichen hängen nur noch ran)
awk '/xx component/{flag=1;next} /yy component/{flag=0} flag' control | tr 'D' 'E' | paste -s -d ' ' > dxyz.out
awk '/yy component/{flag=1;next} /zz component/{flag=0} flag' control | tr 'D' 'E' | paste -s -d ' ' >> dxyz.out
awk '/zz component/{flag=1;next} /xy component/{flag=0} flag' control | tr 'D' 'E' | paste -s -d ' ' >> dxyz.out
awk '/xy component/{flag=1;next} /xz component/{flag=0} flag' control | tr 'D' 'E' | paste -s -d ' ' >> dxyz.out
awk '/xz component/{flag=1;next} /yz component/{flag=0} flag' control | tr 'D' 'E' | paste -s -d ' ' >> dxyz.out
awk '/yz component/{flag=1;next} /raman spectrum/{flag=0} flag' control | tr 'D' 'E' | paste -s -d ' ' >> dxyz.out

exit 0

